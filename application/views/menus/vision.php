<style>
  p {
    font-family: Arial;
    font-size: 16px;
  }
</style>
<center>
<h1>VISIÓN</h1>
<pre>

  Brindar a nuestros comensales una experiencia culinaria única e inolvidable,
  a través de la innovación en nuestros platos, la excelencia en nuestro servicio
  y la calidad de nuestros ingredientes, promoviendo siempre un ambiente acogedor
  y amigable que invite a regresar".
</pre>
</center>
