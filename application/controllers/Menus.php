<?php
/**
 * }
 */
class Menus extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

  }

  public function desayunos(){
    $this->load->view('header');
    $this->load->view('menus/desayunos');
    $this->load->view('footer');
  }
  public function almuerzos(){
    $this->load->view('header');
    $this->load->view('menus/almuerzos');
    $this->load->view('footer');
  }
  public function meriendas(){
    $this->load->view('header');
    $this->load->view('menus/meriendas');
    $this->load->view('footer');
  }
  public function clientes(){
    $this->load->view('header');
    $this->load->view('menus/clientes');
    $this->load->view('footer');
  }
  public function platofuerte(){
    $this->load->view('header');
    $this->load->view('menus/platofuerte');
    $this->load->view('footer');
  }
  public function mision(){
    $this->load->view('header');
    $this->load->view('menus/mision');
    $this->load->view('footer');
  }
  public function vision(){
    $this->load->view('header');
    $this->load->view('menus/vision');
    $this->load->view('footer');
  }
  public function ubicacion(){
    $this->load->view('header');
    $this->load->view('menus/ubicacion');
    $this->load->view('footer');
  }

}
